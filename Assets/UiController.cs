using TMPro;
using UniRx;
using UnityEngine;

namespace DefaultNamespace
{
    public class UiController: MonoBehaviour
    {
        public TextMeshProUGUI pointDisplay;
        public TextMeshProUGUI pointsDisplay;

        private void Start()
        {
            PlayerBehaviour.ScoreSubject.Subscribe((e) =>
            {
                this.pointsDisplay.text = $"Score: {e}";
            });
        }

        public void DisplayHit()
        {
            this.pointDisplay.enabled = true;
            this.Invoke(nameof(HideDisplay),3);
        }

        private void HideDisplay() => this.pointDisplay.enabled = false;
    }
}