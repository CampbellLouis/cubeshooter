﻿using DefaultNamespace;
using UniRx;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public UiController uiController;
    public Camera playerCamera;
    private int _score = 0;
    public static readonly Subject<int> ScoreSubject = new Subject<int>();
    
    private void Start() => this.playerCamera = Camera.main;

    private void Update()
    {
        if (Input.touchCount == 0) return;
        Ray ray = this.playerCamera.ScreenPointToRay(Screen.safeArea.center);
        if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity)) return;
        Destroy(hit.transform.gameObject);
        this.uiController.DisplayHit();
        _score += 50;
        ScoreSubject.OnNext(_score);
    }

    ~PlayerBehaviour() => ScoreSubject.Dispose();
    
}