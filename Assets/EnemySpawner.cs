﻿using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    // Start is called before the first frame update
    private void Start()
    {
        foreach (int el in Enumerable.Range(0,10)) 
            Instantiate(enemy, new Vector3(Random.Range(0.2f, 2f), 0.5f, Random.Range(0.2f, 2f)), Quaternion.identity);
    }
}
